# corrupted.website

## How to start

```
flask run
```

## Deploy labyrinth frontend

```bash
cd labyrinth-frontend
npm install
# npm start # test 
./deploy.sh
cd ..
flask run
```