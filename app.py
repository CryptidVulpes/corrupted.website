from flask import Flask, render_template, request
from whitenoise import WhiteNoise
from filelock import FileLock

import random
import base64
import datetime

app = Flask(__name__)
app.wsgi_app = WhiteNoise(app.wsgi_app, root='static/')
app.config["TEMPLATES_AUTO_RELOAD"] = True


@app.route('/')
def root():
    return render_template('index.html')


@app.route('/photo')
def photo():
    photo_path = "static/photo.jpg"
    with FileLock('photo.lock'):
        with open(photo_path, 'rb') as fp:
            payload = bytearray(fp.read())

        total_bits = len(payload) * 8
        index = random.randint(0, len(payload) - 1)
        bitshift = random.randint(0, 7)
        payload[index] ^= 1 << bitshift

        with open(photo_path, 'wb') as fp:
            fp.write(payload)

        with open("photo.count", 'r') as fp:
            count = int(fp.readline())
            count += 1

        with open("photo.count", 'w') as fp:
            fp.write(f"{count}")

    remote_ip = request.environ.get(
        'HTTP_X_FORWARDER_FOR', request.remote_addr
    )
    flipped_bit = index * 8 + bitshift
    timestamp = datetime.datetime.utcnow()
    return render_template(
        'photo.html',
        _all_bits=total_bits, _current_bits=count,
        _cache_bust=timestamp.timestamp(), _creator=remote_ip,
        _timestamp=timestamp, _flipped_bit=flipped_bit
    )

@app.route('/labyrinth')
def labyrinth():
    return render_template('labyrinth.html')


@app.route('/advertisement')
def advertisement():
    return render_template('advertisement.html')


@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html')