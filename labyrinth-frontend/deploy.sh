npm run build
cp -f build/index.html ../templates/labyrinth.html
cp -rf build ../static/
mv -f ../static/build/*.png ../static/
mv -f ../static/build/*.ico ../static/
rm -f ../static/build/index.html
