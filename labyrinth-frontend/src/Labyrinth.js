import React from 'react';
import { Row, Col, Typography } from 'antd';
// import { Beforeunload } from 'react-beforeunload';
import "../node_modules/antd/dist/antd.css";

const { Text, Title, Paragraph } = Typography;

class Labyrinth extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			status: 'playing',
			height: 11,
			width: 11,
			data: [
				['.', 'x', '.', '.', '.', 'x', '.', '.', '.', 'x', '.'],
				['.', 'x', '.', 'x', '.', 'x', '.', 'x', '.', 'x', '.'],
				['.', 'x', '.', 'x', '.', 'x', '.', 'x', '.', 'x', '.'],
				['.', 'o', '.', 'x', '.', 'o', '.', 'x', '.', 'o', '.'],
				['.', 'x', '.', 'o', '.', 'x', '.', 'o', '.', 'x', '.'],
				['.', 'x', 'o', 'x', 'o', 'x', 'o', 'x', 'o', 'x', '.'],
				['.', 'o', '.', 'x', '.', 'o', '.', 'x', '.', 'o', '.'],
				['.', 'x', '.', 'o', '.', 'x', '.', 'o', '.', 'x', '.'],
				['.', 'x', '.', 'x', '.', 'x', '.', 'x', '.', 'x', '.'],
				['.', 'x', '.', 'x', '.', 'x', '.', 'x', '.', 'x', '.'],
				['.', '.', '.', 'x', '.', '.', '.', 'x', '.', '.', 'z']
			],
			h: 0,
			w: 0,
			money: {h: 10, w: 5},
			money_num: 0,
			num: {'x': 40, 'o': 14, '.': 66, 'z': 1}
		};
	}

	size=40;

	componentDidMount() {
		document.addEventListener("keydown", this.onKeyDown);
	}

	set(pos, ch) {
		var new_data = this.state.data;
		new_data[pos.h][pos.w] = ch;
		this.setState({
			data: new_data
		});
	}

	random_pick_position(types) {
		console.log(types);
		var h = 0;
		var w = 0;
		while (true) {
			h = Math.floor(Math.random() * this.state.height);
			w = Math.floor(Math.random() * this.state.width);
			if (h === this.state.h && w === this.state.w) continue;
			if (h === this.state.money.h && w === this.state.money.w) continue;
			if (this.state.data[h][w] === 'z') continue;
			if (!(this.state.data[h][w] in types)) continue;
			// console.log(this.state.data[h][w]);
			// console.log('x' in types);
			break;
		}
		return {h: h, w: w};
	}

	random_swap() {
		var pos1 = this.random_pick_position({'.':'', 'o':'', 'x':''});
		var pos2 = this.random_pick_position({'.':'', 'o':'', 'x':''});
		var new_data = this.state.data;
		var temp = new_data[pos1.h][pos1.w];
		new_data[pos1.h][pos1.w] = new_data[pos2.h][pos2.w];
		new_data[pos2.h][pos2.w] = temp;
		this.setState({
			data: new_data
		});
	}

	random_change(from_ch, to_ch) {
		var types = {};
		types[from_ch] = '';
		var pos = this.random_pick_position(types);
		var new_data = this.state.data;
		new_data[pos.h][pos.w] = to_ch;
		var new_num = this.state.num;
		new_num[from_ch] -= 1;
		new_num[to_ch] += 1;
		this.setState({
			data: new_data,
			num: new_num
		});
	}

	onKeyDown = (e) => {
		if (this.state.num['.'] < 5) {
			alert('The number of normal pile is less than minimum, you FAILED!');
			return;
		}
		var move_flag = false;
		var boom_flag = false;
		switch(e.keyCode) {
			case 116: // F5
				if (this.state.status in {'lose':'', 'playing':''}) {
					alert('Yes we can refresh the game. BUT PLEASE remember, we only have ONE EARTH. There is no way to refresh the earth.');
				}
				break;
			case 32: // space
				boom_flag = true;
				break;
			case 37: // left
				if (this.state.w > 0 
					&& this.state.data[this.state.h][this.state.w-1] !== 'x') {
					this.setState({
						w: this.state.w - 1
					});
					move_flag = true;
				}
				break;
			case 38: // up
				if (this.state.h > 0
					&& this.state.data[this.state.h-1][this.state.w] !== 'x') {
					this.setState({
						h: this.state.h - 1
					});
					move_flag = true;
				}
				break;
			case 39: // right
				if (this.state.w < this.state.width - 1
					&& this.state.data[this.state.h][this.state.w+1] !== 'x') {
					this.setState({
						w: this.state.w + 1
					});
					move_flag = true;
				}
				break;
			case 40: // down
				if (this.state.h < this.state.height-1 
					&& this.state.data[this.state.h+1][this.state.w] !== 'x') {
					this.setState({
						h: this.state.h + 1
					});
					move_flag = true;
				}
				break;
			default:
				break;
		}
		if (move_flag) {
			if (this.state.data[this.state.h][this.state.w] === ".") {
				this.random_swap();
			} else if (this.state.data[this.state.h][this.state.w] === "o") {
				this.random_change('.', 'o');
				this.random_change('.', 'x');
				this.random_swap();
				this.random_swap();
			} 
			if (this.state.h === this.state.money.h && this.state.w === this.state.money.w) {
				var new_money_pos = this.random_pick_position({'.':'', 'o':''});
				console.log(new_money_pos);
				this.setState({
					money: new_money_pos,
					money_num: this.state.money_num + 1
				});
			}
			if (this.state.data[this.state.h][this.state.w] === 'z') {
				alert('Good job! You reached the destination and won extra ' + this.state.money_num + ' coins');
				this.setState({
					status: 'win'
				});
				window.location.reload();
			}
		}
		if (boom_flag) {
			var boom_num = 0;
			if (this.state.h > 0 && this.state.data[this.state.h-1][this.state.w] === 'x') {
				boom_num += 1;
				this.set({h:this.state.h-1, w:this.state.w}, 'o');
			}
			if (this.state.h < this.state.height-1 && this.state.data[this.state.h+1][this.state.w] === 'x') {
				boom_num += 1;
				this.set({h:this.state.h+1, w:this.state.w}, 'o');
			}
			if (this.state.w > 0 && this.state.data[this.state.h][this.state.w-1] === 'x') {
				boom_num += 1;
				this.set({h:this.state.h, w:this.state.w-1}, 'o');
			}
			if (this.state.w < this.state.width-1 && this.state.data[this.state.h][this.state.w+1] === 'x') {
				boom_num += 1;
				this.set({h:this.state.h, w:this.state.w+1}, 'o');
			}
			for (var i=0; i<boom_num; i++) {
				this.random_change('.', 'x');
				this.random_change('.', 'o');
				this.random_swap();
			}
		}
		if (this.state.num['.'] < 5) {
			alert('The number of normal pile is less than minimum, you FAILED! Press F5 to start again.');
			this.setState({
				status: 'lose'
			});
		}
	}

	render() {
		// brick: https://pixelworlds.fandom.com/wiki/Red_Brick
		// floor: https://www.16pic.com/pic/pic_8860377.html
		var labyrinth = []
		for (var i=0; i<this.state.height; i++) {
			var labyrinth_row = []
			for (var j=0; j<this.state.width; j++) {
				if (this.state.data[i][j] === 'x') {
					labyrinth_row.push(<img src="./brick.png" style={{width:this.size, height:this.size}}/>);
				} else if (this.state.data[i][j] === 'o') {
					if (i === this.state.h && j === this.state.w) {
						labyrinth_row.push(<img src="./floor-danger-human.png" style={{width:this.size, height:this.size}}/>);
					} else if (i === this.state.money.h && j === this.state.money.w) {
						labyrinth_row.push(<img src="./floor-danger-money.png" style={{width:this.size, height:this.size}}/>)
					} else {
						labyrinth_row.push(<img src="./floor-danger.png" style={{width:this.size, height:this.size}}/>);
					}
				} else if (this.state.data[i][j] === 'z') {
					labyrinth_row.push(<img src="./destination.png" style={{width:this.size, height:this.size}}/>);
				} else {
					if (i === this.state.h && j === this.state.w) {
						labyrinth_row.push(<img src="./floor-human.png" style={{width:this.size, height:this.size}}/>);
					} else if (i === this.state.money.h && j === this.state.money.w) {
						labyrinth_row.push(<img src="./floor-money.png" style={{width:this.size, height:this.size}}/>)
					} else {
						labyrinth_row.push(<img src="./floor.png" style={{width:this.size, height:this.size}}/>);
					}
				}
			}
			labyrinth.push(<Row style={{height:this.size}}>{labyrinth_row}</Row>);
		}
		return (
			<div>
				{
				//<Beforeunload onBeforeunload={() => "You'll lose your data!"} />
				}
				<Row style={{height:40}}></Row>
				<Row>
					<Col span={3}></Col>
					<Col span={12} style={{textAlign:"center"}}>
						<Row>
							<Title level={1} style={{color:"lightgray"}}>Anthroprocene: Labyrinth</Title>
						</Row>
						<Row>
						{
							labyrinth
						}
						</Row>
					</Col>
					<Col span={6}>
						<img src="./human.png" style={{width:this.size, height:this.size}}/>
						<Text strong style={{color:"gray", fontSize:24}}>    You Are Here</Text>
						<Paragraph style={{color:"gray", fontSize:1}}></Paragraph>
						<br />
						<img src="./destination.png" style={{width:this.size, height:this.size}}/>
						<Text strong style={{color:"gray", fontSize:24}}>    Destination</Text>
						<Paragraph style={{color:"gray", fontSize:1}}></Paragraph>
						<br />
						<img src="./floor-money.png" style={{width:this.size, height:this.size}}/>
						<Text strong style={{color:"gray", fontSize:24}}>    Money</Text>
						<Paragraph style={{color:"gray", fontSize:1}}></Paragraph>
						<br />
						<img src="./floor.png" style={{width:this.size, height:this.size}}/>
						<Text strong style={{color:"gray", fontSize:24}}>    Normal Pile</Text>
						<Paragraph style={{color:"gray"}}>Every step makes a random swap.</Paragraph>
						<br />
						<img src="./floor-danger.png" style={{width:this.size, height:this.size}}/>
						<Text strong style={{color:"gray", fontSize:24}}>    Danger Pile</Text>
						<Paragraph style={{color:"gray"}}>Every step turns 2 "normal pile" into 1 "danger pile" 
						and 1 "brick pile", then swaps 2 times randomly.</Paragraph>
						<br />
						<img src="./brick.png" style={{width:this.size, height:this.size}}/>
						<Text strong style={{color:"gray", fontSize:24}}>    Brick Pile</Text>
						<Paragraph style={{color:"gray"}}>Usually we cannot pass these brick piles. 
						Unless we press "SPACE" to turn all x brick piles close to us to be "danger piles", 
						and turn 2x "normal piles" into x "danger piles" and x "brick piles", and swaps x times randomly. </Paragraph>
						<br />
					</Col>
					<Col span={3}></Col>
				</Row>
        	</div>
		);
	}
}

export default Labyrinth;